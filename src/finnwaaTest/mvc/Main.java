package finnwaaTest.mvc;

//Main starts the programm

public class Main {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		try {
			View view = new View();
			Model model = new Model();
			Controller control = new Controller(view, model);
			view.setVisible(true);
			view.setLocationRelativeTo(null);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
