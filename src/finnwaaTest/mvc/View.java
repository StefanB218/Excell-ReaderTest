package finnwaaTest.mvc;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class View extends JFrame {

	//UI Elements & Variables
	private static final long serialVersionUID = 1244039777056596227L;
	private JPanel contentPane;
	private JButton btnLschen = new JButton("L\u00F6schen");
	private JMenuItem mntmDateioffnen = new JMenuItem("Datei \u00D6ffnen");
	private JMenu mnDatei = new JMenu("Datei");
	private JMenuBar menuBar = new JMenuBar();
	private JMenuItem mntmDateiSpeichern = new JMenuItem("Datei Speichern");
	public static DefaultListModel<String> listModel = new DefaultListModel<String>();

	/**
	 * Creates the Jframe (UI)
	 */
	public View() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);

		setJMenuBar(menuBar);

		menuBar.add(mnDatei);
		mnDatei.add(mntmDateioffnen);
		mnDatei.add(mntmDateiSpeichern);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(btnLschen, BorderLayout.NORTH);
		JList<String> list = new JList<String>(listModel);
		contentPane.add(list, BorderLayout.CENTER);
	}

	//Checks if JList on UI is full or not
	public static boolean isListFull() {
		try {
			boolean isFull = false;
			if (listModel.isEmpty()) {
				isFull = false;
			}
			else if (!listModel.isEmpty()) {
				isFull = true;
			}
			return isFull;
		}
		catch (Exception e) {
			displayErrorMessage(e.getMessage());
			return false;
		}
	}

	//Clears JList in UI
	public static void clearList() {
		listModel.clear();
	}

	//Opens popup with Message
	public static void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(null, errorMessage);
	}

	//Waits for input -> Datei Oefnen
	void addOpenFileButton(ActionListener acPeformed) {
		mntmDateioffnen.addActionListener(acPeformed);
	}

	//Waits for input -> Datei Speichers
	void addSaveFileButton(ActionListener acPeformed) {
		mntmDateiSpeichern.addActionListener(acPeformed);
	}

	//Waits for input -> Datei Loeschen
	void addDeleteFileButton(ActionListener acPeformed) {
		btnLschen.addActionListener(acPeformed);
	}

	//Adds results to JList so it can be displayed
	public static void fillJList(String path, ArrayList<String> data) {
		if (!data.isEmpty()) {
			listModel.addElement("---------- Anfang: " + path + " ----------");
			for (String line : data) {
				listModel.addElement(line);
			}
			listModel.addElement("---------- Ende: " + path + " ----------");
		}
		else {
			displayErrorMessage("Keine Rechenoperationen gefunden!");
		}
	}
}
