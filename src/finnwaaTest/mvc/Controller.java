package finnwaaTest.mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// The Controller coordinates interactions
// between the View and Model

public class Controller {

	private View view;
	private Model model;

	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;

		this.view.addOpenFileButton(new OpenListener());
		this.view.addSaveFileButton(new SaveListener());
		this.view.addDeleteFileButton(new DeleteListener());

		//Tells the View that when ever the button is clicked
		//to execute the actionPerformed method
		//in one of the classes bellow
	}

	//Classes bellow wait for button click in View 
	//and execute their method in Model
	class OpenListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				model.openFile();
			}
			catch (Exception e2) {
				View.displayErrorMessage(e2.getMessage());
			}
		}
	}

	class SaveListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				model.saveFile();
			}
			catch (Exception e2) {
				View.displayErrorMessage(e2.getMessage());
			}
		}
	}

	class DeleteListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				model.delete();
			}
			catch (Exception e2) {
				View.displayErrorMessage(e2.getMessage());
			}
		}
	}
}
