package finnwaaTest.mvc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//The Model contains all the needed methods

public class Model {

	private JFileChooser fileChooser = new JFileChooser();
	private FileNameExtensionFilter filter = new FileNameExtensionFilter("ONLY Excel Files (xlsx)", new String[] { "xlsx" });

	private ScriptEngineManager mgr = new ScriptEngineManager();
	private ScriptEngine engine = mgr.getEngineByName("JavaScript");

	public ArrayList<String> resultList = new ArrayList<String>();
	public ArrayList<String> operationList = new ArrayList<>();

	//opens Filemanager dialog
	public void openFile() {
		try {
			fileChooser.setFileFilter(filter);
			operationList.clear();
			resultList.clear();
			int result = fileChooser.showOpenDialog(null);
			if (result == 0) {
				//Reads file
				readFile(fileChooser.getSelectedFile());
			}
		}
		catch (Exception e) {
			View.displayErrorMessage(e.getMessage());
		}
	}

	//opens Filemanager save dialog
	public void saveFile() {
		try {
			if (!View.listModel.isEmpty()) {
				fileChooser.setFileFilter(filter);
				int userSelection = fileChooser.showSaveDialog(fileChooser);

				if (userSelection == JFileChooser.APPROVE_OPTION) {
					File fileToSave = fileChooser.getSelectedFile();
					writeFile(fileToSave);
				}
			}
			else if (resultList.isEmpty())
				View.displayErrorMessage("Datei muss eingelesen werden bevor gespeichert werden kann!");
		}
		catch (Exception e) {
			View.displayErrorMessage(e.getMessage());
		}
	}

	//Deletes content of JList & results
	//so it can do new calculations again
	public void delete() {
		try {
			boolean isFull = View.isListFull();
			if (isFull == true) {
				View.clearList();
				resultList.clear();
			}
			else if (isFull == false) {
				View.displayErrorMessage("Es wurde noch nichts eingelesen");
			}
		}
		catch (Exception e) {
			View.displayErrorMessage(e.getMessage());
		}
	}

	//Called by openFile
	public void readFile(File pathToFile) {
		try {
			//Opens selected file
			FileInputStream inputStream = new FileInputStream(pathToFile);

			//Opens Excel-file at sheet 0
			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();

			//Iterates through file and collects its content			
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				Iterator<Cell> cellIterator = currentRow.iterator();

				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					operationList.add(currentCell.toString());
					//adds currentCell to List
				}
			}
			workbook.close();
			inputStream.close();
			processData(pathToFile.getName().toString());
			//handsover ArrayList to UI so it can display the Results
		}
		catch (Exception e) {
			View.displayErrorMessage(e.getMessage());
		}
	}

	//Called by SaveFile
	//Opens Save dialog
	public void writeFile(File fileToSave) {
		try {

			String filename = fileToSave.getAbsolutePath();
			FileOutputStream fileOut = new FileOutputStream(filename);

			String sheetName = "Results";

			XSSFWorkbook wb = new XSSFWorkbook();
			XSSFSheet sheet = wb.createSheet(sheetName);

			//Creates Rows & prints results in columns
			//Gets data from JListmodel
			for (int i = 0; i < View.listModel.size(); i++) {
				XSSFRow row = sheet.createRow(i);

				for (int c = 0; c < 1; c++) {
					XSSFCell cell = row.createCell(c);
					cell.setCellValue(View.listModel.get(i));
				}
			}

			wb.write(fileOut);
			fileOut.flush();
			fileOut.close();
			wb.close();
			View.displayErrorMessage("Erfolreich gespeichert: " + filename.toString());
		}
		catch (Exception e) {
			View.displayErrorMessage(e.getMessage());
		}
	}

	//Called by ProcessData
	//Uses JavaScript engine to calculate simple arithmetic formula from a String
	public Object calculateString(String operation) {
		try {
			return engine.eval(operation);
		}
		catch (Exception e) {
			return null;
		}
	}

	//Called by ReadFile
	//Processes data from OperationList
	//Adds results to ResultList
	public void processData(String path) {
		try {
			String calculation = null;
			Object result = null;

			for (String cellContent : operationList) {

				result = calculateString(cellContent);
				calculation = cellContent + "=" + result;

				if (result != null) {
					//Adds results to ArrayList
					resultList.add(calculation);
				}
			}
			View.fillJList(path, resultList);
		}
		catch (Exception e) {
			View.displayErrorMessage(e.getMessage());
		}
	}

}
