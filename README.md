# Excell Reader
# Reupload von 2018
Aufgabe:
Überarbeiten Sie die bereits abgegebene Software in der Art, dass sie einem MVC-Pattern entspricht. Strukturieren Sie das Programm weiter so um, dass statische Referenzen nur an den Stellen verwendet werden, wo es Sinn macht. Achten Sie auch darauf, konsequent einen objektorientieren Ansatz zu verfolgen.
