package finnwaaTest.mvc;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class ModelTest {

	//JUnit tests Open, Read, Write, Save functions
	private Model classToTest;

	@Before
	public void setUp() throws Exception {
		classToTest = new Model();
	}

	@Test
	public void testReadWriteFile() throws Exception {
		ArrayList<String> operationTEST = classToTest.operationList;
		ArrayList<String> resultTEST = classToTest.resultList;
		classToTest.openFile();
		if (!operationTEST.isEmpty()) {
			assertTrue(true, "ReadFile");
			classToTest.saveFile();
			if (!resultTEST.isEmpty()) {
				assertTrue(true, "WriteFile");
			}
			else {
				assertFalse(true);
			}
		}
		else {
			assertFalse(true);
		}
	}
}
